<?php
	/**
	** This method is in charge of handle and create another APK on the fly.
	*/

	header("Access-Control-Allow-Origin: *");

	ini_set("display_errors",1);
	$url = $_REQUEST['url'];
	define("SRC_PATH", "/Users/alejandronarancio/Desktop/");

	define("WINDOW_BACKGROUND", "#ECECEC");
	define("WINDOW_PRIMARY", "#3F51B5");
	define("WINDOW_PRIMARY_DARK", "#303F9F");
	define("WINDOW_COLOR_ACCENT", "#3F51B5");
	define("WINDOW_FONT_COLOR", "#FFFFFF");

	if (isset($url) && $url != '') { 

		$url = $url."wp-json/wp-android/options";

		//Get the content of the json and options from the file
		$curl = curl_init($url);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

	    $str = curl_exec($curl);
		$json = json_decode($str, true);

		$wp_to_android_app_name = $json['wp_to_android_app_name'];
		$wp_to_android_app_email = $json['wp_to_android_app_email'];
		$wp_to_android_app_host = $json['wp_to_android_app_host'];
		$wp_to_android_app_anspress = $json['wp_to_android_app_anspress'];
		$wp_to_android_app_bbpress = $json['wp_to_android_app_bbpress'];
		$wp_to_android_app_woocommerce = $json['wp_to_android_app_woocommerce'];

		if ($wp_to_android_app_name == "") {
			echo "Android Application Name is mandatory";
		}

		if ($wp_to_android_app_host == "") {
			echo "Android Application Host is mandatory";
		}

		$wp_to_android_app_back_color = $json['wp_to_android_app_back_color'];
		$wp_to_android_app_pri_color = $json['wp_to_android_app_pri_color'];
		$wp_to_android_app_pri_dark_color = $json['wp_to_android_app_pri_dark_color'];
		$wp_to_android_app_accent_color = $json['wp_to_android_app_accent_color'];
		$wp_to_android_app_font_color = $json['wp_to_android_app_font_color'];

		$wp_to_android_app_icon_48 = $json['wp_to_android_app_icon_48'];
		$wp_to_android_app_splash_48 = $json['wp_to_android_app_splash_48'];
		$wp_to_android_app_navigation_48 = $json['wp_to_android_app_navigation_48'];

		$keystore_file_url = $json['wp_to_android_keystore'];
		$store_password = $json['wp_to_android_store_password'];
		$key_alias = $json['wp_to_android_key_alias'];
		$key_password = $json['wp_to_android_key_password'];

		$wp_to_android_version_code = $json['wp_to_android_version_code'];
		$wp_to_android_version_name = $json['wp_to_android_version_name'];
		$siteurl = $json['siteurl'];

		$wp_to_android_app_ads = $json['wp_to_android_app_ads'];
		$wp_to_android_app_admob = $json['wp_to_android_app_admob'];
		$wp_to_android_ad_footer_id = $json['wp_to_android_ad_footer_id'];
		$wp_to_android_ad_middle_id = $json['wp_to_android_ad_middle_id'];
		$wp_to_android_ad_header_id = $json['wp_to_android_ad_header_id'];

		$wp_to_android_ad_dfp_footer_id = $json['wp_to_android_ad_dfp_footer_id'];
		$wp_to_android_ad_dfp_middle_id = $json['wp_to_android_ad_dfp_middle_id'];
		$wp_to_android_ad_dfp_header_id = $json['wp_to_android_ad_dfp_header_id'];

		$wp_to_android_posts_menu = $json['wp_to_android_posts_menu'];
		$wp_to_android_categories_menu = $json['wp_to_android_categories_menu'];
		$wp_to_android_users_menu = $json['wp_to_android_users_menu'];
		$wp_to_android_pages_menu = $json['wp_to_android_pages_menu'];
		$wp_to_android_galleries_menu = $json['wp_to_android_galleries_menu'];

		// Create the structure of the new project
		createNewFolderStructure(str_replace(" ", "-", $wp_to_android_app_name));
		// Change the properties File
		changePropertyFile($wp_to_android_app_name, $wp_to_android_app_host, $wp_to_android_app_anspress, $wp_to_android_app_bbpress, $wp_to_android_app_ads, $wp_to_android_app_admob, $wp_to_android_ad_footer_id, $wp_to_android_ad_middle_id,  $wp_to_android_ad_header_id, $wp_to_android_ad_dfp_footer_id, $wp_to_android_ad_dfp_middle_id,  $wp_to_android_ad_dfp_header_id, $wp_to_android_posts_menu, $wp_to_android_categories_menu, $wp_to_android_users_menu, $wp_to_android_pages_menu, $wp_to_android_galleries_menu);
		// Change the color of the application
		changeColors($wp_to_android_app_name, $wp_to_android_app_back_color, $wp_to_android_app_pri_color, $wp_to_android_app_pri_dark_color, $wp_to_android_app_accent_color, $wp_to_android_app_font_color);
		// Change icon of application
		changeIcons($wp_to_android_app_name, $wp_to_android_app_icon_48);
		// Change Splash of application
		changeSplash($wp_to_android_app_name, $wp_to_android_app_splash_48);
		// Change Nav Header of application
		changeNavHeader($wp_to_android_app_name, $wp_to_android_app_navigation_48);
		// Change the signigng keys credentials (if we have this setup)
		
		$appName = str_replace(" ", "-", $wp_to_android_app_name);

		if ($store_password != "" && $key_alias != "" && $key_password != "") {
			changeSigningKey($siteurl."/".$keystore_file_url, $wp_to_android_app_name, $store_password, $key_alias, $key_password, $wp_to_android_version_code, $wp_to_android_version_name);
			buildApplicationSigned($appName);
			$pathToFile = SRC_PATH.$appName."/app/build/outputs/apk/app-release.apk";
		} else { 
			buildApplication($appName);
			$pathToFile = SRC_PATH.$appName."/app/build/outputs/apk/app-debug.apk";
		}
		
		while( !file_exists($pathToFile) )
		{
		    sleep(1);
		}
		//optimizeAPK($pathToFile);

		while( !file_exists($pathToFile) )
		{
		    sleep(1);
		}
		//sendEmail($pathToFile, $wp_to_android_app_email, $wp_to_android_app_name);

		echo "success";

	} else {
		echo "There was an error with the process, please try it again!";
	}

	function optimizeAPK($src) {
		shell_exec("redex ".$src." -o ".$src);
	}

	function sendEmail($src, $email) {

		require_once('class.phpmailer.php');

		$bodytext = "Hey! This is your apk file! <br/><br/> ";

		$email = new PHPMailer();
		$email->From      = 'tincholiguori@gmail.com';
		$email->FromName  = 'WP Admin';
		$email->Subject   = 'Here it is your apk! Enjoy it! - Wp Team!';
		$email->Body      = $bodytext;
		$email->AddAddress( $wp_to_android_app_email );

		$file_to_attach = $src;
		$email->AddAttachment( $file_to_attach , $wp_to_android_app_name.'.apk' );

		return $email->Send();

	}

	function buildApplicationSigned($src) {
		shell_exec("chmod +rx ".SRC_PATH.$src);
		//TODO change the ownerships of the files
		shell_exec("chown -R alejandronarancio:staff ".SRC_PATH.$src);
		chdir(SRC_PATH.$src);
		shell_exec("chmod +x gradlew");
		shell_exec("./gradlew assembleRelease");
	}

	function buildApplication($src) {

		shell_exec("chmod +rx ".SRC_PATH.$src);
		//TODO change the ownerships of the files
		shell_exec("chown -R alejandronarancio:staff ".SRC_PATH.$src);
		chdir(SRC_PATH.$src);
		shell_exec("chmod +x gradlew");
		shell_exec("./gradlew assembleDebug");
	}

	function changeColors($wp_to_android_app_name, $wp_to_android_app_back_color, $wp_to_android_app_pri_color, $wp_to_android_app_pri_dark_color, $wp_to_android_app_accent_color, $wp_to_android_app_font_color){

		$colorsStr = '<?xml version="1.0" encoding="utf-8"?><resources>';
		if ($wp_to_android_app_back_color != "") $colorsStr .= '<color name="windowBackground">'.$wp_to_android_app_back_color.'</color>';
		else $colorsStr .= '<color name="windowBackground">'.WINDOW_BACKGROUND.'</color>';

		if ($wp_to_android_app_pri_color != "") $colorsStr .= '<color name="colorPrimary">'.$wp_to_android_app_pri_color.'</color>';
		else $colorsStr .= '<color name="colorPrimary">'.WINDOW_PRIMARY.'</color>';

		if ($wp_to_android_app_pri_dark_color != "") $colorsStr .= '<color name="colorPrimaryDark">'.$wp_to_android_app_pri_dark_color.'</color>';
		else $colorsStr .= '<color name="colorPrimaryDark">'.WINDOW_PRIMARY_DARK.'</color>';

		if ($wp_to_android_app_accent_color != "") $colorsStr .= '<color name="colorAccent">'.$wp_to_android_app_back_color.'</color>';
		else $colorsStr .= '<color name="colorAccent">'.WINDOW_COLOR_ACCENT.'</color>';

		if ($wp_to_android_app_font_color != "") $colorsStr .= '<color name="fontColorForPrimaryBackground">'.$wp_to_android_app_back_color.'</color>';
		else $colorsStr .= '<color name="fontColorForPrimaryBackground">'.WINDOW_FONT_COLOR.'</color>';

		$colorsStr .= '<color name="transparent_black">#7f000000</color><color name="lightgrey">#D6D6D6</color><color name="white">#FFFFFF</color><color name="orange">#ffa500</color><color name="black">#000000</color><color name="image_view_gray">#7F7F7F</color><color name="dark_grey_title">#313131</color><color name="dark_grey_text">#404040</color></resources>';

		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		file_put_contents(SRC_PATH.$app_name_folder."/app/src/main/res/values/colors.xml", $colorsStr);

	}

	function changeIcons($wp_to_android_app_name, $wp_to_android_app_icon) {
		
		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		if ($wp_to_android_app_icon != '') {
			saveFileWithDimension($app_name_folder, $wp_to_android_app_icon, "mipmap-mdpi", "ic_launcher.png", 48, 48);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_icon, "mipmap-hdpi", "ic_launcher.png", 72, 72);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_icon, "mipmap-xhdpi", "ic_launcher.png", 96, 96);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_icon, "mipmap-xxhdpi", "ic_launcher.png", 144, 144);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_icon, "mipmap-xxxhdpi", "ic_launcher.png", 144, 144);
		}
	}

	function changeSplash($wp_to_android_app_name, $wp_to_android_app_splash){
		
		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		if ($wp_to_android_app_splash != '') {
			saveFileWithDimension($app_name_folder, $wp_to_android_app_splash, "drawable-mdpi", "splash_screen.png", 480, 320);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_splash, "drawable-hdpi", "splash_screen.png", 720, 480);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_splash, "drawable-xhdpi", "splash_screen.png", 960, 640);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_splash, "drawable-xxhdpi", "splash_screen.png", 1440, 960);
		}
	}

	function changeNavHeader($wp_to_android_app_name, $wp_to_android_app_navigation){

		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		if ($wp_to_android_app_navigation != '') {
			saveFileWithDimension($app_name_folder, $wp_to_android_app_navigation, "drawable-mdpi", "nav_menu_header.png", 320, 180);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_navigation, "drawable-hdpi", "nav_menu_header.png", 480, 270);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_navigation, "drawable-xhdpi", "nav_menu_header.png", 640, 360);
			saveFileWithDimension($app_name_folder, $wp_to_android_app_navigation, "drawable-xxhdpi", "nav_menu_header.png", 960, 540);
		}
	}
	
	/**
	* Packages the aplication
	**/
	function zipApplication($src) {

		$src = SRC_PATH.$src;
		// Get real path for our folder
		$rootPath = realpath($src);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($src.'.apk', ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);

		foreach ($files as $name => $file) {
		    // Skip directories (they would be added automatically)
		    if (!$file->isDir()) {
		        // Get real and relative path for current file
		        $filePath = $file->getRealPath();
		        $relativePath = substr($filePath, strlen($rootPath) + 1);

		        // Add current file to archive
		        $zip->addFile($filePath, $relativePath);
		    }
		}

		// Zip archive will be created only after closing object
		$zip->close();
	}

	function changeSigningKey($keystore_file_url, $wp_to_android_app_name, $store_password, $key_alias, $key_password, $version_code, $version_name) {

		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		$stringXML = file_get_contents(SRC_PATH.$app_name_folder."/app/build.gradle");

		$stringXML = str_replace("STORE_PASSWORD", $store_password, $stringXML);
		$stringXML = str_replace("KEY_ALIAS", $key_alias, $stringXML);
		$stringXML = str_replace("KEY_PASSWORD", $key_password, $stringXML);
		$stringXML = str_replace("VERSION_CODE", $version_code, $stringXML);
		$stringXML = str_replace("VERSION_NAME", $version_name, $stringXML);
		
		file_put_contents(SRC_PATH.$app_name_folder."/app/build.gradle", $stringXML);

		$content = file_get_contents($keystore_file_url);
		$fp = fopen(SRC_PATH.$app_name_folder."/app/apk/myreleasekey.keystore", "w");
		fwrite($fp, $content);
		fclose($fp);
	}

	function changePropertyFile($wp_to_android_app_name, $wp_to_android_app_host, $wp_to_android_app_anspress, $wp_to_android_app_bbpress, $wp_to_android_app_ads, $wp_to_android_app_admob, $wp_to_android_ad_footer_id, $wp_to_android_ad_middle_id,  $wp_to_android_ad_header_id, $wp_to_android_ad_dfp_footer_id, $wp_to_android_ad_dfp_middle_id,  $wp_to_android_ad_dfp_header_id, $wp_to_android_posts_menu, $wp_to_android_categories_menu, $wp_to_android_users_menu, $wp_to_android_pages_menu, $wp_to_android_galleries_menu) {
	
		// Changing property file of the application
		$string = "host.baseUrl=";
		$string .= $wp_to_android_app_host."\n";
		//$string .= "http://todolosdeportes.infuy.com/"."\n";
		$string .= "version=";
		$string .= "premium"."\n";
		$string .= "plugins=";

		if ($wp_to_android_app_anspress == 1 && $wp_to_android_app_bbpress == 1) {
			$string .= "anspress,bbpress,woocommerce";
		} else if ($wp_to_android_app_anspress == 1) { 
			$string .= "anspress";
		} else if ($wp_to_android_app_bbpress == 1) { 
			$string .= "bbpress";
		} else if ($wp_to_android_app_woocommerce == 1) { 
			$string .= "woocommerce";
		}
		$string .= "\n";

		$string .= "api.url=wp-json/wp-android"."\n";	

		if ($wp_to_android_app_ads == 1) {
			$string .= "ads=true";
		} else {
			$string .= "ads=false";
		}
		$string .= "\n";

		if ($wp_to_android_app_admob == 1) {
			$string .= "ads_type=admob";
		} else {
			$string .= "ads_type=dfp";
		}
		$string .= "\n";

		$string .= "menu_options";
		if ($wp_to_android_posts_menu == "" &&
			$wp_to_android_categories_menu == "" &&
			$wp_to_android_users_menu == "" &&
			$wp_to_android_pages_menu == "" &&
			$wp_to_android_galleries_menu == "") {
			$string .= "posts,categories,users,pages,galleries";
		} else {
			if ($wp_to_android_posts_menu == 1) {
				$string .= "posts,";
			}
			if ($wp_to_android_categories_menu == 1) {
				$string .= "categories,";
			}
			if ($wp_to_android_users_menu == 1) {
				$string .= "users,";
			}
			if ($wp_to_android_pages_menu == 1) {
				$string .= "pages,";
			}
			if ($wp_to_android_galleries_menu == 1) {
				$string .= "galleries";
			}
			if ($string[strlen($string)] == ",") {
				$string = substr($string, 0, strlen($string) -1);
			}
		}

		$app_name_folder  = str_replace(" ", "-", $wp_to_android_app_name);
		$file = SRC_PATH.$app_name_folder."/app/src/main/assets/app.properties";
		file_put_contents($file, $string);

		// Now i will change the application name
		$stringXML = file_get_contents(SRC_PATH.$app_name_folder."/app/src/main/res/values/strings.xml");
		$stringXML = str_replace("Wp Plugin", $wp_to_android_app_name, $stringXML);

		if ($wp_to_android_app_ads == 1) {
			$stringXML = str_replace("ca-app-pub-3583518377968987/8156387359", $wp_to_android_ad_header_id, $stringXML);
			$stringXML = str_replace("ca-app-pub-3583518377968987/1823978959", $wp_to_android_ad_middle_id, $stringXML);
			$stringXML = str_replace("ca-app-pub-3583518377968987/6760379351", $wp_to_android_ad_footer_id, $stringXML);

			$stringXML = str_replace("DFP1", $wp_to_android_ad_dfp_header_id, $stringXML);
			$stringXML = str_replace("DFP2", $wp_to_android_ad_dfp_middle_id, $stringXML);
			$stringXML = str_replace("DFP3", $wp_to_android_ad_dfp_footer_id, $stringXML);
		}
		file_put_contents(SRC_PATH.$app_name_folder."/app/src/main/res/values/strings.xml", $stringXML);

		// Now i will change the gradle
		$buildGradle = file_get_contents(SRC_PATH.$app_name_folder."/app/build.gradle");
		$buildGradle = str_replace(".wpplugin", ".".str_replace(" ", "_", strtolower($wp_to_android_app_name)), $buildGradle);
		file_put_contents(SRC_PATH.$app_name_folder."/app/build.gradle", $buildGradle);

	}

	function createNewFolderStructure($wp_to_android_app_name) {

		$src = SRC_PATH."app-release/";
		$dst = SRC_PATH.$wp_to_android_app_name;

		full_copy($src, $dst);
		shell_exec("chmod 777 ".$dst);
	}

	/**
	* Functions that creates and copy the new distribution
	**/
	function full_copy( $source, $target ) {
	    if ( is_dir( $source ) ) {
	        @mkdir( $target );
	        $d = dir( $source );
	        while ( FALSE !== ( $entry = $d->read() ) ) {
	            if ( $entry == '.' || $entry == '..' ) {
	                continue;
	            }
	            $Entry = $source . '/' . $entry; 
	            if ( is_dir( $Entry ) ) {
	                full_copy( $Entry, $target . '/' . $entry );
	                continue;
	            }
	            copy( $Entry, $target . '/' . $entry );
	        }

	        $d->close();
	    }else {
	        copy( $source, $target );
	    }
	}

	function saveFileWithDimension($app_name_folder, $wp_to_android_image_path, $dimension, $filename, $width, $height) {

		//First upload a temporary file

		$content = file_get_contents($wp_to_android_image_path);
		$fp = fopen("/tmp/".$filename, "w");
		fwrite($fp, $content);
		fclose($fp);

		$imagick = new \Imagick("/tmp/".$filename);
		$imagick->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
		$imagick->writeImage("/tmp/".$filename);
		//$imagick->destroy(); 

		copy( "/tmp/".$filename, SRC_PATH.$app_name_folder."/app/src/main/res/".$dimension."/".$filename );

		// $content = file_get_contents($wp_to_android_image_path);
		// $fp = fopen(SRC_PATH.$app_name_folder."/app/src/main/res/".$dimension."/".$filename, "w");
		// fwrite($fp, $content);
		// fclose($fp);
	}

?>